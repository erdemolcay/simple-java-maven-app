# Build webserver
FROM golang:1.18-buster AS build
WORKDIR /app
COPY webserver/* ./
RUN go build -o /webserver

# Java app
FROM openjdk:11-jre-slim
COPY --from=build /webserver /webserver
COPY ./target/app.jar /usr/local/lib/app.jar
EXPOSE 3000
ENTRYPOINT ["/webserver", "java","-jar","/usr/local/lib/app.jar"]