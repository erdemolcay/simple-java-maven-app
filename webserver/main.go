package main

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	baseCmd := os.Args[1]
	cmdArgs := os.Args[2:]
	out, err := exec.Command(baseCmd, cmdArgs...).Output()
	if err != nil {
		fmt.Println(err)
	}
	_, err = w.Write(out)
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", indexHandler)
	_ = http.ListenAndServe(":3000", mux)
}
