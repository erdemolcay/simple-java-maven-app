pipeline {
    agent {
        kubernetes {
            inheritFrom 'default'
            yaml '''
            spec:
                containers:
                  - name: maven
                    image: 'maven:3.8-jdk-8-slim'
                    command:
                    - cat
                    tty: true
                  - name: kaniko
                    image: gcr.io/kaniko-project/executor:debug
                    command:
                    - /busybox/cat
                    tty: true
                    volumeMounts:
                    - name: registry-secret
                      mountPath: /kaniko/.docker
                  - name: helm
                    image: alpine/helm:3.10.2
                    command:
                    - cat
                    tty: true
                    volumeMounts:
                    - name: registry-secret
                      mountPath: /root/.config/helm/registry
                volumes:
                - name: registry-secret
                  secret:
                    secretName: registry-credentials
                    items:
                    - key: .dockerconfigjson
                      path: config.json
            '''
        }
    }
    stages {
        stage('Maven Test') { 
            steps {
                container('maven') {
                    sh 'mvn test'
                }
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml' 
                }
            }
        }
        stage('Maven Build') {
            steps {
                container('maven') {
                    sh 'mvn -B -DskipTests clean package'
                }
            }
        }
        stage('Docker Build') {
            steps {
                container('kaniko') {
                    sh "/kaniko/executor --dockerfile `pwd`/Dockerfile --context `pwd` --destination=${env.REGISTRY_SERVER}/app:${env.BUILD_ID}"
                    sh "/kaniko/executor --dockerfile `pwd`/Dockerfile --context `pwd` --destination=${env.REGISTRY_SERVER}/app:latest"
                }
            }
        }
        stage('Helm Upgrade') {
            steps {
                container('helm') {
                    sh "helm upgrade app --install --force  --set registry_server=${env.REGISTRY_SERVER} --set hostname=${env.APP_HOSTNAME} --set tag=${env.BUILD_ID} --namespace app helm"
                }
            }
        }
    }
}