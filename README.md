# simple-java-maven-app

This is an example app for the [K8s DevOps Demo](https://gitlab.com/erdemolcay/k8s-devops-demo) project.

The repository contains a simple Java application which outputs the string "Hello world!" and is accompanied by a couple of unit tests to check that the main application works as expected. The results of these tests are saved to a JUnit XML report.

Additionally a simple [webserver](webserver) is implemented in GoLang to expose the app to web and demonstrate multi-stage Docker builds.